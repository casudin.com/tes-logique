<?php

namespace App\Http\Controllers;

use App\MemberType;
use Illuminate\Http\Request;
use App\User;
use Spatie\Permission\Models\Role;
use Validator;
use DataTables;
use DB;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.user.index');
    }
    public function list(Request $req)
    {
        $data = User::with(['profile', 'credit'])->get();

        return Datatables::of($data)
            ->addIndexColumn()
            ->escapeColumns([])


            ->addColumn('type', function ($data) {
                $id = $data->profile->member_type_id;
                $t = MemberType::where('id', $id)->first();
                return $t->name;
            })
            ->editColumn('created_at', function ($data) {
                return tgl_indo($data->created_at);
            })
            ->addColumn('gender', function ($data) {
                if ($data->profile->gender == 1) {
                    return "Laki-laki";
                } else {
                    return "Perempuan";
                }
            })
            ->addColumn('pajak', function ($data) {
                $id = $data->profile->member_type_id;
                $umur = umur($data->profile->dob);
                $jk = $data->profile->gender;
                $t = MemberType::where('id', $id)->first();
                if ($id == 2 and $umur > 16 and $jk == 2) {
                    return "Bebas Pajak";
                } else {
                    return $t->tax . ' %';
                }
                if ($id == 1 and $umur > 19 and $jk == 2) {
                    return "Bebas Pajak";
                } else {
                    return $t->tax . ' %';
                }
                if ($id == 3 and $umur > 21 and $jk == 2) {
                    return "Bebas Pajak";
                } else {
                    return $t->tax;
                }
            })
            ->addColumn('cc', function ($data) {
                return $data->credit->no_cc;
            })
            ->editColumn('status', function ($data) {
                if ($data->status == 1) {
                    return 'Aktif';
                } else {
                    return 'Suspend';
                }
            })

            ->editColumn('first_name', function ($data) {
                return $data->first_name . " " . $data->last_name . " (" . umur($data->profile->dob) . "th)";
            })

            ->addColumn('action', function ($data) {
                return '<div class="dropdown custom-dropdown">
                                                        <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                            <i class="flaticon-dot-three"></i>
                                                        </a>

                                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink1">

                                                            <a class="dropdown-item" href="' . route('user.edit', $data->uuid) . '" data-toggle="modal" data-target="#myModal">Edit</a>
                                                            <a class="dropdown-item" onclick="hapus(' . "'$data->id'" . ')">Delete</a>

                                                        </div>
                                                    </div>';
            })

            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $role = Role::orderBy('name', 'ASC')->get();
        return view('admin.user.create', compact('role'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        $valid = Validator::make($req->all(), [
            'nama' => 'required|string|max:100',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
            'role' => 'required|exists:roles,name'
        ]);
        if ($valid->fails()) {
            return response()->json(['status' => 'error', 'code' => 400, 'msg' => $valid->messages()->first()]);
        }
        $user = User::firstOrCreate([
            'email' => $req->email
        ], [
            'name' => $req->nama,
            'password' => bcrypt($req->password),
            'status' => true
        ]);
        $user->assignRole([$req->role]);
        return response()->json(['status' => 'success', 'msg' => "Berhasil"]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = User::where('uuid', $id)->first();
        $role = Role::all()->pluck('name');
        return view('admin.user.edit', compact('data', 'role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $valid = Validator::make($request->all(), [
            'nama' => 'required|string|max:100',
            'email' => 'required|email|exists:users,email',
            'password' => 'nullable|min:6',
            'role' => 'required|exists:roles,name'
        ]);
        if ($valid->fails()) {
            return response()->json(['status' => 'error', 'code' => 400, 'msg' => $valid->messages()->first()]);
        }
        $user = User::findOrFail($id);
        $password = !empty($request->password) ? bcrypt($request->password) : $user->password;
        $user->update([
            'name' => $request->nama,
            'password' => $password,
            'status' => $request->status
        ]);
        $user->syncRoles([$request->role]);
        return response()->json(['status' => 'success', 'msg' => "Berhasil"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //User::destroy($id);

        //return response()->json(['status' => 'success', 'msg' => "Berhasil"]);
    }
}
