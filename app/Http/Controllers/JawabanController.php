<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class JawabanController extends Controller
{
    public function prima(Request $req)
    {
        if ($req->isMethod("POST")) {
            $x = $this->cekBilangan(request()->bilangan_prima);
            if ($x) {
                return response()->json(['success' => true, 'data' => request()->bilangan_prima]);
            } else {
                return response()->json(['success' => false, 'data' => request()->bilangan_prima]);
            }
        } else {
            return view('jawaban.prima');
        }
    }
    public function cekBilangan($number)
    {
        if ($number == 1) {
            return false;
        }

        if ($number == 2) {
            return true;
        }

        $x = sqrt($number);
        $x = floor($x);
        for ($i = 2; $i <= $x; ++$i) {
            if ($number % $i == 0) {
                break;
            }
        }

        if ($x == $i - 1) {
            return true;
        } else {
            return false;
        }
    }
    public function bintang()
    {
        for ($i = 1; $i <= 10; $i++) {
            # code...
            for ($j = 4; $j >= $i; $j--) {
                # code...
                echo "&nbsp;&nbsp;";
            }

            for ($k = 1; $k <= $i; $k++) {
                # code...
                echo "$k";
            }
            echo "<br>";
        }
    }
}
