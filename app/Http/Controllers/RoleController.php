<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use DataTables;
use Spatie\Permission\Models\Permission;
use DB;

class RoleController extends Controller
{
    public function index()
    {
        $this->authorize('super admin');
        return view('admin.acces.index');
    }
    public function roleList()
    {
        $data = Role::all();
        $roles = Role::all()->pluck('name');

        return Datatables::of($data)
            ->addIndexColumn()
            ->escapeColumns([])
            ->addColumn('per', function ($data) {

                //apabila parameter role terpenuhi
                $nama = [];
                //select role berdasarkan namenya, ini sejenis dengan method find()
                $getRole = Role::findByName($data->name);
                //Query untuk mengambil permission yang telah dimiliki oleh role terkait
                $hasPermission = DB::table('role_has_permissions')
                    ->select('permissions.name')
                    ->join('permissions', 'role_has_permissions.permission_id', '=', 'permissions.id')
                    ->where('role_id', $getRole->id)->get()->pluck('name')->all();
                $permissions = Permission::all()->pluck('name');
                $no = 1;
                foreach ($hasPermission as $key) {
                    $nama[] = $key . "<br>";
                }
                return  $nama;
            })


            ->editColumn('created_at', function ($data) {
                return tgl_indo($data->created_at);
            })
            ->editColumn('updated_at', function ($data) {
                return tgl_indo($data->updated_at);
            })



            ->addColumn('action', function ($data) {
                return '<i class="flaticon-delete-fill icon label-danger" onclick="hapusRole(' . "'$data->id'" . ')"></i>
                <a href="' . route('role.permisiion', ['role' => $data->name]) . '" data-toggle="modal" data-target="#myModal"><i class="flaticon-lock-3 icon label-danger" title="Assign Permission" ></i></a>';
            })
            ->setTotalRecords(100)
            ->make(true);
    }
    public function create()
    {
        return view('admin.acces.role_create');
    }
    public function store(Request $req)
    {
        $valid = \Validator::make($req->all(), [
            'name' => 'required|string|max:50'
        ]);
        if ($valid->fails()) {
            return response()->json(['status' => 'error', 'code' => 400, 'msg' => $valid->messages()->first()]);
        }
        $role = Role::firstOrCreate($req->all());
        return response()->json(['status' => 'success', 'msg' => "Berhasil"]);
    }
    public function rolePermission(Request $request)
    {
        $role = request()->role;

        //Default, set dua buah variable dengan nilai null
        $permissions = null;
        $hasPermission = null;

        //Mengambil data role
        $roles = Role::all()->pluck('name');

        //apabila parameter role terpenuhi
        if (!empty($role)) {
            //select role berdasarkan namenya, ini sejenis dengan method find()
            $getRole = Role::findByName($role);

            //Query untuk mengambil permission yang telah dimiliki oleh role terkait
            $hasPermission = DB::table('role_has_permissions')
                ->select('permissions.name')
                ->join('permissions', 'role_has_permissions.permission_id', '=', 'permissions.id')
                ->where('role_id', $getRole->id)->get()->pluck('name')->all();

            //Mengambil data permission
            $permissions = Permission::all()->pluck('name');
        }
        return view('admin.acces.role_permission', compact('roles', 'permissions', 'hasPermission'));
    }
    public function setRolePermission(Request $request)
    {
        //select role berdasarkan namanya
        $role = Role::findByName(request()->role);

        //fungsi syncPermission akan menghapus semua permissio yg dimiliki role tersebut
        //kemudian di-assign kembali sehingga tidak terjadi duplicate data
        $role->syncPermissions($request->permission);
        return response()->json(['status' => 'success', 'msg' => "Berhasil Assign Permission"]);
    }
    public function update(Request $req, $id)
    {
    }
    public function destroy($id)
    {
        Role::destroy($id);
        return response()->json(['status' => 'success', 'msg' => "Berhasil"]);
    }
}
