<?php

namespace App\Http\Controllers;

use App\Addres;
use App\CreditCard;
use Illuminate\Http\Request;
use Auth;
use App\MemberType;
use App\Profile;
use Carbon\Carbon;
use App\User;
use DB;
use Validator;

class AuthApiController extends Controller
{
    public function login(Request $req)
    {
        try {
            $valid = Validator::make($req->all(), [

                'email' => 'required|email|max:255|regex:/(.*)@gmail\.com/i',
                'password' => 'required',

            ]);
            if ($valid->fails()) {
                return response()->json(['status' => 'error', 'msg' => $valid->messages()->first()]);
            }



            if (Auth::guard('web')->attempt(['email' => $req->email, 'password' => $req->password])) {
                return response()->json([
                    'success' => true
                ], 200);
            } else {
                return response()->json([
                    'success' => false,
                    'msg' => 'Login Gagal!'
                ], 401);
            }






            // all good
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'msg' => $e->getMessage()]);
        }
    }
    public function daftar(Request $req)
    {
        if ($req->ajax()) {
            DB::beginTransaction();




            try {
                $valid = \Validator::make($req->all(), [
                    'last_name' => 'required',
                    //'email' => 'required|email|max:255|unique:users',
                    'email' => 'required|email|max:255|regex:/(.*)@gmail\.com/i|unique:users',
                    'password' => 'required|min:6',
                    'jk' => 'required',
                    'dob' => 'date',
                    'credit' => 'required',
                    'member' => 'required',
                    'typeCard' => 'required',
                    'term' => 'required',

                ]);
                if ($valid->fails()) {
                    return response()->json(['status' => 'error', 'msg' => $valid->messages()->first()]);
                }
                $user = User::Create([
                    'email' =>  request()->email,
                    'first_name' =>  request()->first_name,
                    'last_name' => request()->last_name,
                    'password' => bcrypt($req->password),
                    'status' => true,
                    'term' => request()->term
                ]);
                Profile::Create(['user_id' => $user->id, 'dob' => request()->dob, 'gender' => request()->jk, 'member_type_id' => request()->member]);

                CreditCard::Create([
                    'user_id' => $user->id,
                    'no_cc' => request()->credit,
                    'expired_date' => request()->expired,
                    'type' => request()->typeCard
                ]);
                $data = [];
                foreach ($req->data as $key => $val) {
                    $data[] = [
                        'user_id' => $user->id,
                        'value' => $val['alamat']
                    ];
                }

                DB::table('address')->insert($data);


                DB::commit();
                return response()->json(['success' => true, 'msg' => "Selamat Kamu berasil Daftar Menjadi member"]);
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json(['status' => 'error', 'msg' => $e->getMessage()]);
            }
        } else {
            $member = MemberType::all();
            return view('auth.register', compact('member'));
        }
    }
    public function pajak(Request $req)
    {
        try {
            $valid = Validator::make($req->all(), [

                'dob' => 'required',
                'jk' => 'required',

            ]);
            if ($valid->fails()) {
                return response()->json(['status' => 'error', 'msg' => $valid->messages()->first()]);
            }

            $data = MemberType::where('id', request()->type)->first();
            $umur = $this->hitung(request()->dob);
            $pajak = "";
            $biaya = 0;
            $total = 0;
            if (request()->type == 2 and $umur > 16 and request()->jk == 2) {
                $pajak = "Bebas Pajak";
                $biaya = 0;
                $total = 0;
            } else {
                $pajak = $data->tax . "%";
                $biaya = $data->value;
                $total = $data->value + ($data->value  * $data->tax / 100);
            }
            if (request()->type == 1 and $umur > 19 and request()->jk == 2) {
                $pajak = "Bebas Pajak";
                $biaya = 0;
                $total = 0;
            } else {
                $pajak = $data->tax . "%";
                $biaya = $data->value;
                $total = $data->value + ($data->value  * $data->tax / 100);
            }
            if (request()->type == 3 and $umur > 21 and request()->jk == 2) {
                $pajak = "Bebas Pajak";
                $biaya = 0;
                $total = 0;
            } else {
                $pajak = $data->tax . "%";
                $biaya = $data->value;
                $total = $data->value + ($data->value  * $data->tax / 100);
            }
            return response()->json(['status' => 'success', 'pajak' => $pajak, 'biaya' => $biaya, 'total' => $total]);





            // all good
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'msg' => $e->getMessage()]);
        }
    }
    public function hitung($date)
    {
        $now = Carbon::now(); // Tanggal sekarang
        $b_day = Carbon::parse($date); // Tanggal Lahir
        $age = $b_day->diffInYears($now);
        return $age;
    }
    public function cekalamat($alamat)
    {
        $dataSet = [];
        foreach ($alamat as $key) {
            $dataSet[] = [

                'value'    => $key,

            ];
        }

        return  $dataSet;
    }
}
