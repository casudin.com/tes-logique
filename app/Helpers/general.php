<?php

use Ramsey\Uuid\Uuid;
use Carbon\Carbon;

if (!function_exists('DummyFunction')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function DummyFunction()
    {
    }
}
function bahasa()
{
    return request()->session()->get('locale');
}
function unik()
{
    return Uuid::uuid4()->toString();
}
function userRole($id)
{
    $user = App\User::find($id);
    return    $user->roles->pluck('name');
}
function romawi($num)
{
    $n = intval($num);
    $res = '';

    $roman_numerals = array(
        'M'  => 1000,
        'CM' => 900,
        'D'  => 500,
        'CD' => 400,
        'C'  => 100,
        'XC' => 90,
        'L'  => 50,
        'XL' => 40,
        'X'  => 10,
        'IX' => 9,
        'V'  => 5,
        'IV' => 4,
        'I'  => 1
    );

    foreach ($roman_numerals as $roman => $number) {
        $matches = intval($n / $number);

        $res .= str_repeat($roman, $matches);

        $n = $n % $number;
    }

    return $res;
}
function convert_ke_bulan($idbulan, $full = false)
{
    $shortName = array(
        'Jan',
        'Feb',
        'Mar',
        'Apr',
        'Mei',
        'Jun',
        'Jul',
        'Ags',
        'Sep',
        'Okt',
        'Nov',
        'Des',
    );

    if ($full) {
        $fullName = array(
            'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember',
        );

        return $fullName[$idbulan - 1];
    }

    return $shortName[$idbulan - 1];
}
function tgl_indo($tgldb, $separator_asal = '-', $separator_tujuan = ' ', $full = false)
{
    if (empty($tgldb) && (strlen($tgldb) < 4)) {
        return null;
    }
    /* $tgldb formatnya 2015-05-29 , rubah menjadi 29-Mei-2015 */
    /* cek apakah mengandung jam atau detik, panjang max = 10 karakter */
    $tgldb = substr($tgldb, 0, 10);

    $tgl = explode($separator_asal, $tgldb);
    if (!(count($tgl) > 1)) {
        return $tgldb;
    }
    $newTgl = array(
        $tgl[2],
        convert_ke_bulan($tgl[1], $full),
        $tgl[0],
    );

    return implode($separator_tujuan, $newTgl);
}
function umur($date)
{
    $now = Carbon::now(); // Tanggal sekarang
    $b_day = Carbon::parse($date); // Tanggal Lahir
    $age = $b_day->diffInYears($now);
    return $age;
}
