<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Addres extends Model
{
    protected $table = "address";
    protected $fillable = [
        'user_id', 'value'
    ];
}
