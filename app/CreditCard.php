<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CreditCard extends Model
{
    protected $table = "credit_cards";
    protected $fillable = [
        'no_cc', 'user_id', 'type', 'expired_date'
    ];
}
