<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="robots" content="noindex, nofollow">
        <meta name="robots" content="noindex, nofollow">
        <title>{{config('app.name')}} :: Login</title>
        <link rel="icon" type="image/x-icon" href="assets/img/favicon.ico" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/plugins.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/users/login-2.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <link rel="stylesheet" href="{{ asset('alert/dist/sweetalert2.min.css') }}">
        <link href="{{ asset('assets/css/ui-kit/tabs-accordian/custom-tabs.css') }}" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css"
            href="{{ asset('plugins/jqvalidation/custom-jqBootstrapValidation.css') }}">
    </head>

    <body class="login">

        <div class="col-sm-12 layout-spacing">
            <div class="statbox widget box box-shadow">
                <div class="widget-header border-bottom border-default">
                    <div class="row">
                        <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                            <h4>Halaman Pendaftaran</h4>
                        </div>
                    </div>
                </div>
                <div class="widget-content widget-content-area">
                    <form id="daftar" method="POST">
                        <div id="p-1" class="underline-content">
                            <ul class="nav nav-tabs  mb-3 justify-content-between" id="progress" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link nav-item active btn-rounded" id="rounded-pills-home-tab3"
                                        data-toggle="tab" href="#progressbar-1" role="tab" aria-selected="false"><i
                                            class="flaticon-user-7"></i>
                                        Data Diri</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link nav-item btn-rounded" id="rounded-pills-profile-tab3"
                                        data-toggle="tab" href="#progressbar-2" role="tab" aria-selected="true"><i
                                            class="flaticon-home-fill-1"></i>
                                        Informasi Tambahan</a>

                                </li>
                                <li class="nav-item">
                                    <a class="nav-link nav-item btn-rounded" id="ronded-pills-contact-tab3"
                                        data-toggle="tab" href="#progressbar-3" role="tab" aria-selected="false"><i
                                            class="flaticon-telephone"></i>
                                        Membership</a>
                                </li>
                            </ul>
                            <div class="progress progress-md">
                                <div class="progress-bar bg-secondary" role="progressbar" style="" aria-valuenow="25"
                                    aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <div class="tab-content" id="progressContent">
                                <div class="tab-pane fade show active" id="progressbar-1" role="tabpanel">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="fistname3">Nama Depan</label>
                                                <input type="text" class="form-control" id=" " placeholder="Nama Depan"
                                                    name="first_name">
                                            </div>
                                            <div class="form-group">
                                                <label for="">Email</label>
                                                <input type="text" class="form-control" placeholder="Enter email"
                                                    name="email">
                                                <span style="color: red">Hanya mendukung Provider Gmail</span>
                                            </div>
                                            <div class="form-group">
                                                <label for="x">Jenis Kelamin</label>
                                                <select name="jk" class="form-control" id="jk">
                                                    <option value="1" selected>laki-laki</option>
                                                    <option value="2">Perempuan</option>

                                                </select>
                                            </div>

                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for=" ">Nama Belakang</label>
                                                <input type="text" class="form-control" id="fistname3"
                                                    placeholder="Nama Belakang" name="last_name">
                                            </div>
                                            <div class="form-group">
                                                <label for="Password3">Password</label>
                                                <input type="password" class="form-control" id=" "
                                                    placeholder="Password" name="password">
                                            </div>
                                            <div class="form-group">
                                                <label for="">Tanggal Lahir</label>
                                                <input type="date" class="form-control" id="dob" name="dob">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="tab-pane fade" id="progressbar-2" role="tabpanel">


                                    <div class="form-group">
                                        <label for="">Daftar Alamat</label>
                                        <div class="row">
                                            <div class="col-md-6 ">
                                                <div class="repeater-default">
                                                    <div data-repeater-list="data" class="pb-5">

                                                        <div class="r-container" data-repeater-item>
                                                            <div class="form-group mb-5">
                                                                <div class="row">
                                                                    <input type="text"
                                                                        class="form-control-rounded form-control"
                                                                        placeholder="Write Here..." name="alamat">
                                                                    <div class="col-md-2">
                                                                        <span data-repeater-delete
                                                                            class="btn btn-warning">
                                                                            <span
                                                                                class="glyphicon glyphicon-remove"></span>
                                                                            Hapus
                                                                        </span>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="r-container" data-repeater-item>
                                                            <div class="form-group mb-5">
                                                                <div class="row">
                                                                    <input type="text"
                                                                        class="form-control-rounded form-control"
                                                                        placeholder="Write Here..." name="alamat">
                                                                    <div class="col-md-2">
                                                                        <span data-repeater-delete
                                                                            class="btn btn-warning">
                                                                            <span
                                                                                class="glyphicon glyphicon-remove"></span>
                                                                            Hapus
                                                                        </span>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>


                                                    </div>
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <span data-repeater-create
                                                                    class="btn btn-button-7 btn-md">
                                                                    <span class="flaticon-plus  "></span> Tambah Baris
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <hr />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                <div class="tab-pane fade" id="progressbar-3" role="tabpanel">

                                    <div class="form-group">
                                        <label for="exampleFormControlSelect3">Pilih Jenis Member</label>
                                        <select class="form-control" name="member" id="member">
                                            <option value="">--</option>
                                            @forelse ($member as $key)
                                            <option value="{{ $key->id }}">{{ $key->name }}</option>
                                            @empty

                                            @endforelse

                                        </select>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="x">Pajak</label>
                                                <input type="text" class="form-control" id="pajak" placeholder="pajak"
                                                    readonly>


                                            </div>
                                            <div class="form-group">
                                                <label for="location3">Nomor Kartu Kredit</label>
                                                <input type="text" class="form-control" placeholder="Nomor Kartu Kredit"
                                                    name="credit">
                                            </div>
                                            <div class="form-group">
                                                <label for="location3">Tanggal Expired</label>
                                                <input type="date" class="form-control" name="expired">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="x">Biaya</label>
                                                <input type="text" class="form-control" id="biaya" placeholder="biaya"
                                                    readonly>
                                            </div>
                                            <div class="form-group">
                                                <label for="x">Total + PPN</label>
                                                <input type="text" class="form-control" id="total" placeholder="total"
                                                    readonly>
                                            </div>
                                            <div class="form-group">
                                                <label for="location3">Jenis Kartu</label>
                                                <select class="form-control" name="typeCard">
                                                    <option value="">--</option>
                                                    <option value="Visa">Visa</option>
                                                    <option value="Master Card">Master Card</option>

                                                </select>
                                            </div>
                                        </div>
                                    </div>




                                    <label>Dengan Ini saya setuju dengan peraturan <input type="checkbox"
                                            class="form-control" name="term" value="1"></label>


                                </div>

                            </div>
                            <ul class="pagination justify-content-between pager wizard">
                                <li class="previous"><a href="javascript:void(0);"
                                        class="btn-rounded bg-primary btn-primary ml-1">Sebelumnya</a></li>
                                <li class="next"><a href="javascript:void(0);"
                                        class="btn-rounded bg-primary btn-primary ml-1">Selanjutnya</a></li>
                                <li class="finish">
                                    <button class="buttonload btn btn-primary glow w-100 position-relative"
                                        type="button">

                                        <i
                                            class="icon-arrow fa fa-circle-o-notch fa-spin spinner-border text-light"></i>
                                    </button>
                                    <button type="submit"
                                        class="login_btn btn btn-primary glow w-100 position-relative">Daftar
                                        <i id="icon-arrow" class="flaticon-send-arrow"></i></button>


                                </li>
                            </ul>
                        </div>
                </div>
                @csrf
                </form>
            </div>
        </div>
        </div>
        <!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
        <script src="assets/js/libs/jquery-3.1.1.min.js"></script>
        <script src="bootstrap/js/popper.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="{{asset ('alert/dist/sweetalert2.all.min.js') }}"></script>
        <script src="plugins/bootstrap-wizard/jquery.bootstrap.wizard.js"></script>
        <script src="plugins/form-repeater/jquery.repeater.min.js"></script>
        <!-- END GLOBAL MANDATORY SCRIPTS -->
        <script>
            $.ajaxSetup({
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
            });
            $(".buttonload").hide();
         $('#p-1').bootstrapWizard({onTabShow: function(tab, navigation, index) {
                var $total = navigation.find('li').length;
                var $current = index+1;
                var $percent = ($current/$total) * 100;
                $('#p-1 .progress-bar').css({width:$percent+'%'});
                
        }});
        // $('#p-1 .finish').click(function() {
       
        //       $('#p-1').find("a[href*='rounded-pills-home-tab3']").trigger('click');
        // });
        $("#member").change(function (e) { 
            e.preventDefault();
            var type=$("#member").val();
            var dob=$("#dob").val();
            var jk=$("#jk").val();
            $("#pajak").val('');
            $("#biaya").val('');
            $("#total").val('');
            $.ajax({
                type: "POST",
                url: "{{ route("pajak") }}",
                data: {type:type,jk:jk,dob:dob},
                dataType: "JSON",
                success: function (response) {
                    if(response.status=="success"){
                        $("#pajak").val(response.pajak);
                        $("#biaya").val(response.biaya);
                        $("#total").val(response.total);
                       
                    }else if (dob.length=="") {
                    Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: "Silahkan Isi dahulu data tanggal lahir anda",
                    })
                } else{
                        Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: response.msg,
                        })
                    }
                },error:function(response){
                    Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: response.msg,
                    })
                }
            });
            
        });
     
            $("#daftar").submit(function (e) { 
                e.preventDefault();
                $(".buttonload").show();
                $(".login_btn").hide();
                var form=$("#daftar").serialize();
                $.ajax({
                    type: "POST",
                    url: "{{ route("daftar") }}",
                    data: form,
                    dataType: "JSON",
                    success: function (response) {
                      
                        if (!response.success) {
                        Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: response.msg,
                         })
                            $(".login_btn").show();
                            $(".buttonload").hide();
                            
                        } else {
                           Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: response.msg,
                        showConfirmButton: false,
                        timer: 3000
                        }).then (function() {
                            $(".login_btn").hide();
                            $(".buttonload").hide();
                            window.location.href = "{{ route("home") }}";
                            });
                           
                        }
                        
                    },
                     
                    error:function(response){
                       Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: "Terjadi kesalahan, silahkan coba lagi",
                        })
                        $(".login_btn").show();
                        $(".buttonload").hide();
                        
                    }
                });
            });
         $('.repeater-default').repeater({
            show: function () { $(this).slideDown('slow'); }
        });
              
        

        </script>
    </body>

</html>