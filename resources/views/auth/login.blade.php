<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="robots" content="noindex, nofollow">
        <meta name="robots" content="noindex, nofollow">
        <title>{{config('app.name')}} :: Login</title>
        <link rel="icon" type="image/x-icon" href="assets/img/favicon.ico" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/plugins.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/users/login-2.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <link rel="stylesheet" href="{{ asset('alert/dist/sweetalert2.min.css') }}">
    </head>

    <body class="login">

        <form class="form-login" method="POST" id="form-login">
            @csrf
            <div class="row">

                <div class="col-md-12 text-center mb-4">
                    <img alt="logo" src="assets/img/logo-3.png" class="theme-logo">
                </div>
                <div class="flash-message">

                    @foreach (['danger', 'warning', 'success', 'info'] as $msg)

                    @if(Session::has('alert-' . $msg))



                    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close"
                            data-dismiss="alert" aria-label="close">&times;</a></p>

                    @endif

                    @endforeach

                </div>
                <div class="col-md-12">

                    <label for="inputEmail" class="sr-only">Email address</label>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="icon-inputEmail"><i class="flaticon-user-7"></i> </span>
                        </div>
                        <input type="email" id="inputEmail" class="form-control " placeholder=" Email Address"
                            value="{{old('email')}}" autofocus name="email">


                    </div>

                    <label for="inputPassword" class="sr-only">Password</label>
                    <div class="input-group mb-4">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="icon-inputPassword"><i class="flaticon-key-2"></i>
                            </span>
                        </div>
                        <input type="password" id="inputPassword" class="form-control" placeholder="Password"
                            aria-describedby="inputPassword" name="password">

                    </div>

                    <div class="checkbox d-flex justify-content-center mt-3">
                        <div class="custom-control custom-checkbox mr-3">
                            <input type="checkbox" class="custom-control-input" id="customCheck1" name="remember"
                                id="remember" {{ old('remember') ? 'checked' : '' }}>
                            <label class="custom-control-label" for="customCheck1">Remember me</label>
                        </div>
                    </div>

                    <button type="submit" class="login_btn btn btn-primary glow w-100 position-relative">Login
                        <i id="icon-arrow" class="flaticon-send-arrow"></i></button>
                    <button class="buttonload btn btn-primary glow w-100 position-relative" type="button">

                        <i class="icon-arrow fa fa-circle-o-notch fa-spin spinner-border text-light"></i>
                    </button>



                </div>

                <div class="col-md-12">
                    <div class="login-text text-center">
                        <p class="mt-3 text-white">New Here? <a href="{{ route('daftar') }}" class="">Register </a> a
                            new
                            user !</p>
                    </div>
                </div>

            </div>
        </form>
        <!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
        <script src="assets/js/libs/jquery-3.1.1.min.js"></script>
        <script src="bootstrap/js/popper.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="{{asset ('alert/dist/sweetalert2.all.min.js') }}"></script>
        <!-- END GLOBAL MANDATORY SCRIPTS -->
        <script>
            $(".buttonload").hide();
             
            $(".form-login").submit(function (e) { 
                e.preventDefault();
                $(".buttonload").show();
                $(".login_btn").hide();
                var form=$(".form-login").serialize();
                $.ajax({
                    type: "POST",
                    url: "{{ route("api.login") }}",
                    data: form,
                    dataType: "JSON",
                    success: function (response) {
                      
                        if (!response.success) {
                        Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: response.msg,
                         })
                            $(".login_btn").show();
                            $(".buttonload").hide();
                            
                        } else {
                           Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: 'Anda akan di arahkan dalam 3 Detik',
                        showConfirmButton: false,
                        timer: 3000
                        }).then (function() {
                            $(".login_btn").hide();
                            $(".buttonload").hide();
                            window.location.href = "{{ route("home") }}";
                            });
                           
                        }
                        
                    },
                     
                    error:function(response){
                       Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: "Email / Password Salah",
                        })
                        $(".login_btn").show();
                        $(".buttonload").hide();
                        
                    }
                });
            });
        </script>
    </body>

</html>