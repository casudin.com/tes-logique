@extends('layouts.master')
@section('title', trans('akses'))
@section('parentPageTitle', 'Admin')
@section('css')

<link rel="stylesheet" type="text/css" href="{{asset('plugins/table/datatable/datatables.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('plugins/table/datatable/custom_dt_customer.css')}}">
@endsection
@section('content')
<div class="main-body">
    <div class="page-wrapper">
        <div class="page-body">
            <div class="row">
                <div class="col-lg-12 col-md-12 layout-spacing">
                    <div class="statbox widget box box-shadow">
                        <div class="widget-header">
                            <br>
                            <div class="row">
                                <div class="col-md-5 col-sm-5 mb-4 mb-sm-0">
                                    <h6 class="mt-3 mb-0"> </h6>
                                </div>
                                <div class="col-md-7 col-sm-7 text-sm-right">
                                    <a href="{{route('role.create')}}" data-toggle="modal" data-target="#myModal"
                                        class="btn btn-warning mb-4 mr-2 btn-rounded">{{trans('tambah')}} <i
                                            class="flaticon-circle-plus ml-1"></i></a>
                                    <a type="button" class="btn btn-primary mb-4 mr-2 btn-rounded">Export <i
                                            class="flaticon-file ml-1"></i></a>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                    <h4>{{trans('role')}}</h4>
                                </div>
                            </div>
                        </div>
                        <div class="widget-content widget-content-area">
                            <div class="table-responsive">
                                <table class="table table-bordered mb-4" id="roles">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>{{trans('nama')}}</th>
                                            <th>Group</th>
                                            <th>{{trans('dibuat')}}</th>
                                            <th>Access</th>
                                            <th>#</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        {{-- <td class="text-center"><i class="flaticon-delete-fill icon"></i></td> --}}

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12 col-md-12 layout-spacing">
                    <div class="statbox widget box box-shadow">
                        <div class="widget-header">
                            <br>
                            <div class="row">
                                <div class="col-md-5 col-sm-5 mb-4 mb-sm-0">
                                    <h6 class="mt-3 mb-0"> </h6>
                                </div>
                                <div class="col-md-7 col-sm-7 text-sm-right">
                                    <a href="{{route('permission.create')}}" data-toggle="modal" data-target="#myModal"
                                        class="btn btn-warning mb-4 mr-2 btn-rounded">{{trans('tambah')}} <i
                                            class="flaticon-circle-plus ml-1"></i></a>
                                    <a type="button" class="btn btn-primary mb-4 mr-2 btn-rounded">Export <i
                                            class="flaticon-file ml-1"></i></a>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                    <h4>{{trans('permission')}}</h4>
                                </div>
                            </div>
                        </div>
                        <div class="widget-content widget-content-area">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table-striped mb-4" id="permission">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>{{trans('nama')}}</th>
                                            <th>Group</th>
                                            <th>{{trans('dibuat')}}</th>
                                            <th>updated_at</th>
                                            <th>#</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        {{-- <td class="text-center"><i class="flaticon-delete-fill icon"></i></td> --}}

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script src="{{asset('plugins/table/datatable/datatables.js')}}"></script>
@endsection
@section('modal')
<div class="modal fade animated zoomInUp custo-zoomInUp" tabindex="-1" role="dialog"
    aria-labelledby="myExtraLargeModalLabel" aria-hidden="true" id="myModal">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
    $("#myModal").on("show.bs.modal", function(e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-content").load(link.attr("href"));
        });
        // $(".select2").select2({
        // placeholder: 'Cari',
        // allowClear: true
        // });
var Roles=$('#roles').DataTable({
        processing: true,
        serverSide: true,
        pageLength:5,
       // lengthMenu:[[5,10,15,20,50,100],[5,10,15,20,50,100]],
        lengthMenu: [ 5, 10, 20, 50, 100 ],
        language: {
                paginate: {
                  previous: "<i class='flaticon-arrow-left-1'></i>",
                  next: "<i class='flaticon-arrow-right'></i>"
                },
                //info: "Showing page _PAGE_ of _PAGES_"
            },
        ajax: {
            url: '{{route("api.role")}}',
            method: 'POST'
        },
        columns: [
            {data: 'DT_RowIndex', orderable: false, searchable: false},
            {data: 'name', name: 'name'},
            {data: 'guard_name', name: 'guard_name'},
            {data: 'created_at', name: 'created_at'},
            {data: 'per', name: 'per'},
            {data: 'action', name: 'action',orderable: false, searchable: false}
        ],

    });
var Permission=$('#permission').DataTable({
        processing: true,
        serverSide: true,
        pageLength:5,
       // lengthMenu:[[5,10,15,20,50,100],[5,10,15,20,50,100]],
        lengthMenu: [ 5, 10, 20, 50, 100 ],
        language: {
                paginate: {
                  previous: "<i class='flaticon-arrow-left-1'></i>",
                  next: "<i class='flaticon-arrow-right'></i>"
                },
                //info: "Showing page _PAGE_ of _PAGES_"
            },
        ajax: {
            url: '{{route("api.permission")}}',
            method: 'POST'
        },
        columns: [
           {data: 'DT_RowIndex', orderable: false, searchable: false},
            {data: 'name', name: 'name'},
            {data: 'guard_name', name: 'guard_name'},
            {data: 'created_at', name: 'created_at'},
            {data: 'updated_at', name: 'updated_at'},
            {data: 'action', name: 'action',orderable: false, searchable: false}
        ],

    });
function hapusRole(id){

    swalWithBootstrapButtons.fire({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Yes, delete it!',
    cancelButtonText: 'No, cancel!',
    reverseButtons: true,
    showLoaderOnConfirm: true,
    }).then((result) => {
  if (result.value) {
      $.ajax({
          type: "POST",
          method:"DELETE",
          url: "{{url('admin/acl/role')}}/"+id,
          data: {id:id},
          dataType: "JSON",
          success: function (response) {
              if (response.status!='error') {
                swalWithBootstrapButtons.fire(
                'Deleted!',
                'Your file has been deleted.',
                'success',
                ''
                );
                Toast.fire({
                icon: 'success',
                title: 'Your Roles has been deleted.'
                });
                Roles.ajax.reload();
              } else {
                swalWithBootstrapButtons.fire(
                    'Error',
                    response.msg,
                    'error'
                    );
              }
          }
      });
  } else if (
    /* Read more about handling dismissals below */
    result.dismiss === Swal.DismissReason.cancel
  ) {
    swalWithBootstrapButtons.fire(
      'Cancelled',
      'Your imaginary User is safe :)',
      'error'
    );
  }
});

 }
 function hapusPermission(id){

    swalWithBootstrapButtons.fire({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Yes, delete it!',
    cancelButtonText: 'No, cancel!',
    reverseButtons: true,
    showLoaderOnConfirm: true,
    }).then((result) => {
  if (result.value) {
      $.ajax({
          type: "POST",
          method:"DELETE",
         url: "{{url('admin/acl/permission')}}/"+id,
          data: {id:id},
          dataType: "JSON",
          success: function (response) {
              if (response.status!='error') {
                  swalWithBootstrapButtons.fire(
                'Deleted!',
                'Your file has been deleted.',
                'success',
                ''
                );
                Toast.fire({
                icon: 'success',
                title: 'Your Permission has been deleted.'
                });
                Permission.ajax.reload();
              } else {

              }
          }
      });
  } else if (
    /* Read more about handling dismissals below */
    result.dismiss === Swal.DismissReason.cancel
  ) {
    swalWithBootstrapButtons.fire(
      'Cancelled',
      'Your imaginary User is safe :)',
      'error'
    );
  }
})

 }

</script>
@endsection
