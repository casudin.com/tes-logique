<div class="modal-header">
    <h5 class="modal-title" id="myExtraLargeModalLabel">Form</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
</div>
<form method="POST" id="form_data">
    <div class="modal-body">
        <div class="form-row mb-4">
            @php $no = 1; @endphp
            <div class="widget-content widget-content-area">
                <div class="row">
                    @foreach ($permissions as $key => $row)
                    <div class="col-md-6 col-6">
                        <div class="n-chk">
                            <label class="new-control new-checkbox new-checkbox-rounded checkbox-outline-info">
                                <input type="checkbox" class="new-control-input" value="{{ $row }}"
                                    {{ in_array($row, $hasPermission) ? 'checked':'' }} name="permission[]">
                                <span class="new-control-indicator"></span>{{ $row }}
                            </label>
                        </div>
                    </div>
                    @if ($no++%4 == 0)
                    <br>
                    @endif
                    @endforeach
                </div>


            </div>


        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-dark btn-rounded mt-3 mb-3" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary btn-rounded mt-3 mb-3">Submit</button>
        </div>
    </div>
</form>
<script>
    $("#form_data").submit(function (e) {
        e.preventDefault();
       $('#myModal').modal('hide')
        var form=$("#form_data").serialize();
       $.ajax({
           type: "POST",
           url: "{{route('role.permisiion.set',['role'=>request()->role])}}",
           data: form,
           dataType: "JSON",
           success: function (response) {
            if (response.status!="error") {
                Toast.fire({
                icon: 'success',
                title: response.msg
                });
                Roles.ajax.reload();
            } else {
                Swal.fire({
                position: 'top-end',
                icon: 'error',
                title: response.msg,
                showConfirmButton: false,
                showCloseButton:true
                })
            }
           },
           error:function(response)
           {
            Swal.fire({
                position: 'top-end',
                icon: 'error',
                title: response.msg,
                showConfirmButton: false,
                showCloseButton:true
                })
           }
       });
    });
</script>
