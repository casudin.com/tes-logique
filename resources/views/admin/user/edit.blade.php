<div class="modal-header">
    <h5 class="modal-title" id="myExtraLargeModalLabel">Form</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
</div>
<form method="POST" id="form_data">
    <div class="modal-body">
        <div class="form-row mb-4">
            <div class="form-group col-md-6">
                <label for="inputEmail4">{{trans('nama')}}</label>
                <input type="text" class="form-control-rounded form-control" name="nama" placeholder="{{trans('nama')}}"
                    value="{{$data->name}}">
            </div>
            <div class="form-group col-md-6">
                <label for="inputEmail4">{{trans('email')??"Email"}}</label>
                <input type="email" class="form-control-rounded form-control" name="email" placeholder="Email"
                    value="{{$data->email}}">
            </div>
            <div class="form-group col-md-6">
                <label for="inputPassword4">Password</label>
                <input type="password" class="form-control-rounded form-control" id="inputPassword4"
                    placeholder="Password" name="password">
                <span>Kosongkan Jika Tidak Di ubah</span>
            </div>
            <div class="form-group col-md-6">
                <label for="inputPassword4">Role</label>
                <select name="role[]" class="select2 form-control-rounded form-control" multiple>
                    <option value=""></option>
                    @foreach ($role as $key)
                    <option value="{{$key}}" {{$data->hasRole($key) ? 'selected':'' }}>{{$key}}</option>

                    @endforeach
                </select>
            </div>
            <div class="form-group col-md-6">
                <label for="inputPassword4">Status</label>
                <select name="status" class="select2 form-control-rounded form-control">
                    <option value="1" {{$data->status==1?"selected":""}}>Active</option>
                    <option value="0" {{$data->status==0?"selected":""}}>Suspend</option>

                </select>
            </div>
        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-dark btn-rounded mt-3 mb-3" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary btn-rounded mt-3 mb-3">Update</button>
        </div>
    </div>
</form>
<script>
    $(".select2").select2({
    placeholder: "Pilih",
    allowClear: true
    });
    $("#form_data").submit(function (e) {
        e.preventDefault();
       $('#myModal').modal('hide')
        var form=$("#form_data").serialize();
       $.ajax({
           type: "POST",
           method:"PUT",
           url: "{{route('user.update',[$data->id])}}",
           data: form,
           dataType: "JSON",
           success: function (response) {
            if (response.status!="error") {
               Toast.fire({
                icon: 'success',
                title: response.msg
                });
                myTable.ajax.reload();

            } else {
                Swal.fire({
                position: 'top-end',
                icon: 'error',
                title: response.msg,
                showConfirmButton: false,
                showCloseButton:true
                })
            }
           },
           error:function(response)
           {
            Swal.fire({
                position: 'top-end',
                icon: 'error',
                title: response.msg,
                showConfirmButton: false,
                showCloseButton:true
                })
           }
       });
    });
</script>
