<ul class="list-unstyled menu-categories" id="accordionExample">
    <li class="menu {{ Request::segment(1) === 'home' ? 'active' : '' }} ">
        <a href="{{route('home')}}" aria-expanded="false" class="dropdown-toggle">
            <div class="{{ Request::segment(1) === 'home' ? 'active' : '' }}">
                <i class="flaticon-home-fill ml-3"></i>
                <span>Home</span>
            </div>


        </a>

    </li>
    <li class="menu">
        <a href="#ecommerce" data-toggle="collapse"
            aria-expanded="{{ Request::segment(1) === 'admin' ? 'true' : 'false' }}" class="dropdown-toggle">
            <div class="">
                <i class="flaticon-lock-4"></i>
                <span>Data</span>
            </div>
            <div>
                <i class="flaticon-right-arrow"></i>
            </div>
        </a>
        <ul class="collapse submenu list-unstyled {{ Request::segment(1) === 'admin' ? 'show' : '' }}  " id="ecommerce"
            data-parent="#accordionExample">
            <li class="{{ Request::segment(2) === 'user' ? 'active' : null }}">
                <a href="{{route('user.index')}}"> {{trans('Member')}} </a>
            </li>
            <li class="{{ Request::segment(3) === 'all' ? 'active' : '' }}">
                <a href="{{route('prima')}}"> {{trans('Jawaban')}} </a>
            </li>


        </ul>
    </li>

</ul>