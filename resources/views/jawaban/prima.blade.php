@extends('layouts.master')
@section('title', trans('Jawaban'))
@section('parentPageTitle', 'Admin')
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('plugins/table/datatable/datatables.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('plugins/table/datatable/custom_dt_customer.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('plugins/select2/select2.min.css')}}">
<style>
    .form-control {
        border: 1px solid #ccc;
        color: #888ea8;
        font-size: 15px;
    }

    code {
        color: #3862f5;
    }

    .form-control:disabled,
    .form-control[readonly] {
        background-color: #f1f3f9;
        border-color: #f1f3f1;
    }

    .btn-primary.disabled,
    .btn-primary:disabled {
        background-color: #3862f5;
        border-color: #3862f5;
    }

    label {
        color: #3b3f5c;
        margin-bottom: 14px;
    }

    .form-control::-webkit-input-placeholder {
        color: #888ea8;
        font-size: 15px;
    }

    .form-control::-ms-input-placeholder {
        color: #888ea8;
        font-size: 15px;
    }

    .form-control::-moz-placeholder {
        color: #888ea8;
        font-size: 15px;
    }

    .form-control:focus {
        border-color: #3862f5;
    }

    .input-group-text {
        background-color: #f3f4f7;
        border-color: #e9ecef;
        color: #6156ce;
    }

    select.form-control {
        display: inline-block;
        width: 100%;
        height: calc(2.25rem + 2px);
        vertical-align: middle;
        background: #fff url(assets/img/arrow-down.png) no-repeat right .75rem center;
        background-size: 13px 14px;
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
    }

    select.form-control::-ms-expand {
        display: none;
    }

</style>
<!--  BEGIN CUSTOM STYLE FILE  -->
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/10.1.2/styles/default.min.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/10.1.2/highlight.min.js"></script>
@endsection
@section('content')
<div class="row">

    <div class="col-lg-12 col-md-12 col-sm-12 mb-4">
        <div class="statbox widget box box-shadow">
            <div class="widget-header widget-heading">
                <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12 layout-spacing">
        <div class="statbox widget box box-shadow">
            <div class="widget-header">
                <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                        <h4>1. Bilangan Prima</h4>
                    </div>
                </div>
            </div>
            <div class="widget-content widget-content-area">
                <form method="post" id="prima">
                    <div class="form-group">

                        <label for="t-text" class="sr-only">Bilangan</label>
                        <input id="t-text" type="number" name="bilangan_prima" class="form-control-rounded form-control"
                            required>


                        <button type="submit" class="login_btn mt-4 btn btn-button-7 btn-rounded">Cek
                            <i id="icon-arrow" class="flaticon-search-1"></i></button>
                        <button class="buttonload mt-4 btn btn-sm btn-button-7 btn-rounded" type="button" disabled>
                            <i class="icon-arrow fa fa-circle-o-notch fa-spin spinner-border text-light"></i>
                        </button>
                        <button type="button" class="btn-warning mt-4 btn btn-rounded" data-toggle="modal"
                            data-target=".bd-example-modal-xl">Cek Kode
                            <i id="icon-arrow" class="flaticon-full-screen"></i></button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12 layout-spacing">
        <div class="statbox widget box box-shadow">
            <div class="widget-header">
                <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                        <h4>2. menentukan bilangan terbesar</h4>
                    </div>
                </div>
            </div>
            <div class="widget-content widget-content-area">
                <form method="post">
                    <div class="form-group">
                        <p><code>$bilangan = array(11,
                        6, 31, 201, 99, 861, 1, 7, 14, 79)</code>.</p>
                        <button type="button" name="cek" id="cek" class="mt-4 btn btn-button-7 btn-rounded">Cek</button>
                        <button type="button" class="btn-warning mt-4 btn btn-rounded" data-toggle="modal"
                            data-target=".bilangan-terbesar">Cek Kode
                            <i id="icon-arrow" class="flaticon-full-screen"></i></button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12 layout-spacing">
        <div class="statbox widget box box-shadow">
            <div class="widget-header">
                <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                        <h4>3. Buatkan fungsi yang dapat menghasilkan format berikut</h4>
                    </div>
                </div>
            </div>
            <div class="widget-content widget-content-area">
                <form method="post">
                    <div class="form-group">
                        <p>Buatkan fungsi yang dapat menghasilkan format berikut</p>
                        @for($i = 1; $i <= 8; $i++) @for($j=4; $j>= $i; $j--)
                            &nbsp;&nbsp;
                            @endfor

                            @for($k = 1; $k
                            <= $i; $k++) {{ $k }} @endfor <br />
                            @endfor


                            <button type="button" class="btn-warning mt-4 btn btn-rounded" data-toggle="modal"
                                data-target=".segitiga">Cek
                                Kode
                                <i id="icon-arrow" class="flaticon-full-screen"></i></button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12 layout-spacing">
        <div class="statbox widget box box-shadow">
            <div class="widget-header">
                <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                        <h4>4. urutkan bilangan-bilangan berikut:</h4>
                    </div>
                </div>
            </div>
            <div class="widget-content widget-content-area">
                <form method="post">
                    <div class="form-group">
                        <p>$bilangan = array(99, 2, 64, 8, 111, 33, 65, 11, 102, 50);</p>

                        <button type="button" name="cek" id="urut"
                            class="mt-4 btn btn-button-7 btn-rounded">Cek</button>
                        <button type="button" class="btn-warning mt-4 btn btn-rounded" data-toggle="modal"
                            data-target=".urutkan">Cek
                            Kode
                            <i id="icon-arrow" class="flaticon-full-screen"></i></button>
                    </div>
                </form>
            </div>
        </div>
    </div>



</div>

@endsection
@section('js')
<script src="{{asset('plugins/table/datatable/datatables.js')}}"></script>
<script src="{{asset('plugins/select2/select2.min.js')}}"></script>

@endsection
@section('modal')
<div class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myExtraLargeModalLabel">Source</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <p>
                    <pre>
                    <code class="php">
                        namespace App\Http\Controllers;

                        use Illuminate\Http\Request;

                        class JawabanController extends Controller
                        {
                        public function prima(Request $req)
                        {
                        if ($req->isMethod("POST")) {
                        $x = $this->cekBilangan(request()->bilangan_prima);
                        if ($x) {
                        return response()->json(['success' => true, 'data' => request()->bilangan_prima]);
                        } else {
                        return response()->json(['success' => false, 'data' => request()->bilangan_prima]);
                        }
                        } else {
                        return view('jawaban.prima');
                        }
                        }
                        public function cekBilangan($number)
                        {
                        if ($number == 1) {
                        return false;
                        }

                        if ($number == 2) {
                        return true;
                        }

                        $x = sqrt($number);
                        $x = floor($x);
                        for ($i = 2; $i <= $x; ++$i) { if ($number % $i==0) { break; } } if ($x==$i - 1) { return true; } else { return false; }
                            } }
                    </code>
                    </pre>
                </p>
                <div class="modal-footer">

                    <button type="button" class="btn btn-dark btn-rounded mt-3 mb-3" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade bilangan-terbesar" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myExtraLargeModalLabel">Source</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <p>
                    <pre>
                    <code class="javascript">
                            $("#cek").click(function (e) { 
                            var object = [1,6, 31, 201, 99, 861, 1, 7, 14, 79];
                            Swal.fire(
                            'Good job!',
                            Math.max.apply(null,object)+ ' adalah Nilai terbesar dari bilangan!',
                            'success'
                            )
                            
                        });
                    </code>
                    </pre>
                </p>
                <div class="modal-footer">

                    <button type="button" class="btn btn-dark btn-rounded mt-3 mb-3" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade segitiga" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myExtraLargeModalLabel">Source</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <p>
                    <pre>
                    <code class="php">
                           for ($i = 1; $i <= 10; $i++) { # code... for ($j=4; $j>= $i; $j--) {
                                # code...
                                echo "&nbsp;&nbsp;";
                                }
                            
                                for ($k = 1; $k <= $i; $k++) { # code... echo "$k" ; } echo "<br>" ; }
                    </code>
                    </pre>
                </p>
                <div class="modal-footer">

                    <button type="button" class="btn btn-dark btn-rounded mt-3 mb-3" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade urutkan" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myExtraLargeModalLabel">Source</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <p>
                    <pre>
                    <code class="javascript">
                         $("#urut").click(function (e) {
                        var object = [99, 2, 64, 8, 111, 33, 65, 11, 102, 50];
                        Swal.fire(
                        'Good job!',
                        ' Berikut Urutan bilangan terkecil hingga terbesar!<br /><b>'+object.sort(function(a, b){return a - b}),
                            'success'
                            )
                            });
                    </code>
                    </pre>
                </p>
                <div class="modal-footer">

                    <button type="button" class="btn btn-dark btn-rounded mt-3 mb-3" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
@section('script')
<script type="text/javascript">
    $(".buttonload").hide();
 $("#prima").submit(function (e) { 
     e.preventDefault();
     $(".buttonload").show();
    $(".login_btn").hide();
    var form=$("#prima").serialize();
      $.ajax({
                    type: "POST",
                    url: "{{ route("prima") }}",
                    data: form,
                    dataType: "JSON",
                    success: function (response) {
                      
                        if (!response.success) {
                        
                         Swal.fire(
                        'Jawaban Kurang tepat!',
                        response.data+ ' Bukan bilangan Prima!',
                        'error'
                        );
                            $(".login_btn").show();
                            $(".buttonload").hide();
                            
                        } else {
                           
                           Swal.fire(
                             'Jawaban Anda benar!',
                            response.data+ ' Merupakan bilangan Prima!',
                                'success'
                        );
                        $(".buttonload").hide();
                        $(".login_btn").show();
                        }
                        
                    },
                     
                    error:function(response){
                       Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: "Error Server",
                        })
                        $(".login_btn").show();
                        $(".buttonload").hide();
                        
                    }
                });
 });
 $("#cek").click(function (e) { 
      var object = [1,6, 31, 201, 99, 861, 1, 7, 14, 79];
    Swal.fire(
    'Good job!',
     Math.max.apply(null,object)+ ' adalah Nilai terbesar dari bilangan!',
    'success'
    )
     
 }); 
 $("#urut").click(function (e) { 
      var object = [99, 2, 64, 8, 111, 33, 65, 11, 102, 50];
    Swal.fire(
    'Good job!',
     ' Berikut Urutan bilangan terkecil hingga terbesar!<br /><b>'+object.sort(function(a, b){return a - b}),
    'success'
    )   
 });
</script>
<script>
    hljs.initHighlightingOnLoad();
</script>
@endsection