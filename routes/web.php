<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});
Route::get('/tes', 'TesController@index');
Route::livewire('/users', 'user.index')->name('users.index');
Auth::routes(['regiter' => true]);
Route::get('/home', 'HomeController@index')->name('home');
Route::group(['middleware' => ['auth', 'status'], 'prefix' => 'admin'], function () {
    Route::resource('user', 'UsersController');
    Route::post('user/api', 'UsersController@list')->name('user.api');
    Route::group(['prefix' => 'acl'], function () {
        Route::get('/', 'RoleController@index')
            ->name('access');
        Route::resource('role', 'RoleController');
        Route::get('show/permission/role', 'RoleController@rolePermission')->name('role.permisiion');
        Route::post('show/permission/role/set', 'RoleController@setRolePermission')->name('role.permisiion.set');
        Route::post('api/role', 'RoleController@roleList')->name('api.role');
        Route::resource('permission', 'PermissionController');
        Route::post('api/permission', 'PermissionController@permissionList')->name('api.permission');
    });
    Route::group(['prefix' => 'jawaban'], function () {
        Route::get('all', 'JawabanController@prima')->name('prima');
        Route::post('all', 'JawabanController@prima')->name('prima');
    });
});
Route::get('lang/{language}', 'LocalizationController@switch')->name('localization.switch');
Route::post('api/login', 'AuthApiController@login')->name('api.login');
Route::get('daftar', 'AuthApiController@daftar')->name('daftar');
Route::post('daftar', 'AuthApiController@daftar')->name('daftar');
Route::post('cek/pajak', 'AuthApiController@pajak')->name('pajak');
